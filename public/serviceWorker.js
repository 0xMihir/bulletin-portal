self.addEventListener("install", event => {
    self.skipWaiting();
    event.waitUntil(
        caches.open("bulletin").then(cache => {
            return cache.addAll([
                "/style.css",
                "/client.js",
                "/transparent-192.png",
                "/manifest.json",
                "/favicon.ico",
                "/shell.html",
                "https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css",
                "https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js",
                "https://apis.google.com/js/client:platform.js?onload=start",
                "https://fonts.googleapis.com/css?family=Roboto:300,400,500|Material+Icons",
                "https://fonts.gstatic.com/s/materialicons/v48/flUhRq6tzZclQEJ-Vdg-IuiaDsNcIhQ8tQ.woff2",
                "https://fonts.gstatic.com/s/roboto/v20/KFOmCnqEu92Fr1Mu4mxKKTU1Kg.woff2",
                "https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmEU9fBBc4AMP6lQ.woff2",
                "material-datetime-picker.js",
                "rome.standalone.js"
            ]);
        })
    );
});

self.addEventListener("activate", event => {
    caches.open("bulletin").then(cache => {
        cache.addAll([
            "/style.css",
            "/client.js",
            "/transparent-192.png",
            "/manifest.json",
            "/favicon.ico",
            "/shell.html",
            "https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css",
            "https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js",
            "https://apis.google.com/js/client:platform.js?onload=start",
            "https://fonts.googleapis.com/css?family=Roboto:300,400,500|Material+Icons",
            "https://fonts.gstatic.com/s/materialicons/v48/flUhRq6tzZclQEJ-Vdg-IuiaDsNcIhQ8tQ.woff2",
            "https://fonts.gstatic.com/s/roboto/v20/KFOmCnqEu92Fr1Mu4mxKKTU1Kg.woff2",
            "https://fonts.gstatic.com/s/roboto/v20/KFOlCnqEu92Fr1MmEU9fBBc4AMP6lQ.woff2",
            "material-datetime-picker.js",
            "rome.standalone.js"
        ]);
    });
    return self.clients.claim();
});
self.addEventListener("fetch", event => {
    const compareUrl = event.request.url.split("?")[0];
    if (new Request("/").url == compareUrl) {
        event.respondWith(
            caches.match("/shell.html").then(e => {
                if (!e) {
                }
                return e || fetch(event.request);
            })
        );
        return;
    }
    if (
        new Request("/announcements").url == event.request.url &&
        event.request.method == "GET"
    ) {
        event.respondWith(
            caches.open("bulletin").then(cache => {
                return fetch(event.request)
                    .then(response => {
                        cache.put(event.request, response.clone());
                        return response;
                    })
                    .catch(e => {
                        console.error(e);
                    });
            })
        );
        return;
    }
    event.respondWith(
        caches.match(event.request).then(response => {
            return (
                response ||
                fetch(event.request).catch(e => {
                    console.error(e);
                })
            );
        })
    );
});

function updateCache(request) {
    return caches.open("bulletin").then(cache => {
        return fetch(request).then(response => {
            return cache.put(request, response.clone()).then(() => {
                return response;
            });
        });
    });
}

self.addEventListener("push", ev => {
    const data = ev.data.json();
    self.registration.showNotification(data.title, {
        body: data.body,
        icon: "/transparent-512.png"
    });
});

self.addEventListener("notificationclick", event => {
    event.notification.close();
    event.waitUntil(
        clients
            .matchAll({
                type: "window"
            })
            .then(clientList => {
                for (var i = 0; i < clientList.length; i++) {
                    var client = clientList[i];
                    if (client.url == "/" && "focus" in client)
                        return client.focus();
                }
                if (clients.openWindow) {
                    return clients.openWindow("/");
                }
            })
    );
});
