var titleInput = document.querySelector("#title"),
    titleField = new mdc.textField.MDCTextField(titleInput.parentElement),
    descInput = document.querySelector("#description"),
    descField = new mdc.textField.MDCTextField(descInput.parentElement),
    dateInput = document.querySelector("#datepicker"),
    datePickerField = new mdc.textField.MDCTextField(dateInput.parentElement),
    dateInput2 = document.querySelector("#datepicker2"),
    datePickerField2 = new mdc.textField.MDCTextField(dateInput2.parentElement),
    startDate,
    endDate,
    date = moment({
        hour: 15
    });

if (moment().hour() > 15) date.add({ day: 1 });

const picker = new MaterialDatetimePicker({
    default: date
})
    .on("close", function() {
        if (datePickerField.value == "") {
            datePickerField.valid = false;
        }
    })
    .on("submit", val => {
        startDate = val;
        datePickerField.value = val.format("LLLL");
    });

const picker2 = new MaterialDatetimePicker({ default: date });

picker2
    .on("close", () => {
        if (datePickerField.value == "") {
            //atePickerField.valid = false;
        }
    })
    .on("submit", val => {
        endDate = val;
        datePickerField2.value = val.format("LLLL");
    });

dateInput.addEventListener("focus", function(e) {
    try {
        picker.open();
    } catch (e) {
        console.error(e);
    }
});

dateInput2.addEventListener("focus", function(e) {
    try {
        picker2.open();
    } catch (e) {
        console.error(e);
    }
});

const clearButton = $("#clear-button");
clearButton.addEventListener("click", function() {
    datePickerField2.value = "";
    endDate = "";
});

const submit = $("#submit");
submit.addEventListener("click", function() {
    let validators = _(".validate");
    let errored = false;
    for (let i = 0; i < validators.length; i++) {
        const e = validators[i];
        if (e.id == "datepicker2") continue;
        if (e.value == "") {
            new mdc.textField.MDCTextField(e.parentElement).valid = false;
            errored = true;
        } else {
            new mdc.textField.MDCTextField(e.parentElement).valid = true;
        }
    }
    if (errored) return;
    //Date validation !
    if (!endDate) {
        endDate = startDate;
    }
    if (endDate.unix() < startDate.unix()) {
        new mdc.textField.MDCTextField(dateInput2.parentElement).valid = false;
        return;
    }
    var body = {
        title: titleField.value,
        description: descInput.value,
        timestamp: startDate.unix(),
        end_timestamp: endDate ? endDate.unix() : startDate.unix()
    };
    console.log("generate body");
    if (typeof id !== "undefined") body.id = id;
    fetch(
        typeof id !== "undefined" ? `/announcements/${id}` : "/announcements",
        {
            method: typeof id !== "undefined" ? "PATCH" : "POST",
            headers: {
                "content-type": "application/json",
                authorization:
                    "Bearer " + window.localStorage.getItem("accessToken")
            },
            body: JSON.stringify(body)
        }
    )
        .then(response => {
            new mdc.snackbar.MDCSnackbar($("#post-success")).open();
            fetch("/announcements");
            setTimeout(() => {
                response.json().then(function(e) {
                    window.location.href = e.url;
                });
            }, 1000);
        })
        .catch(err => {
            console.error(err);
        });
});
