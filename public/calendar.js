const monthNames = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
];

let currentDate = new Date();
var month = currentDate.getMonth();
var year = currentDate.getFullYear();

var yearDisplay = document.getElementById("year");
let days = document.getElementById("days");

let monthDisplay = document.getElementById("monthName");

function clearElement(x) {
    while (x.firstChild) {
        x.removeChild(x.firstChild);
    }
}

function clear() {
    clearElement(yearDisplay);
    clearElement(days);
    clearElement(monthDisplay);
}

function convert(year, month, day) {
    return (
        moment({ year: year, month: month, day: day, hour: 12 }).unix() * 1000
    );
}

function preview(year, month, day) {
    let unixEpochTime = convert(year, month, day);
    window.location.href = `/?timestamp=${unixEpochTime}`;
}

function createTd(num, isActive, monthOffset) {
    let td = document.createElement("td");
    let element = document.createElement("button");
    element.className = "day-selector";
    element.onclick = () => {
        preview(year, month + monthOffset, num);
    };

    if (isActive) {
        element.classList.add("active");
    }

    element.appendChild(document.createTextNode(num));

    td.appendChild(element);
    return td;
}

function calculate() {
    clear();

    let currentMonthDate = new Date(year, month);

    let firstDay = currentMonthDate.getDay();
    let numDays = new Date(year, month + 1, 0).getDate();
    let numDaysOfLastMonth = new Date(year, month, 0).getDate();
    let currentDayOfMonth = currentDate.getDate();

    var arr = [];

    var numDaysAdded = 0;
    var counter = numDaysOfLastMonth;
    for (var i = 0; i < firstDay; i++) {
        arr.unshift(createTd(counter--, false, -1));
        numDaysAdded++;
    }

    for (var i = 1; i <= numDays; i++) {
        arr.push(createTd(i, i == currentDayOfMonth, 0));

        numDaysAdded++;
    }

    for (var i = 1; i <= 35 - numDaysAdded; i++) {
        arr.push(createTd(i, false, 1));
    }

    let display = document.getElementById("toload");
    // display.style.display = 'block'

    yearDisplay.appendChild(document.createTextNode(year));
    monthDisplay.appendChild(document.createTextNode(monthNames[month]));

    for (var i = 0; i < arr.length; i += 7) {
        var row = document.createElement("tr");
        for (var j = i; j < i + 7; j++) {
            row.appendChild(arr[j]);
        }
        days.appendChild(row);
    }
}

function updateMonth(delta) {
    month += delta;

    while (month < 0) {
        year--;
        month += 12;
    }

    while (month >= 12) {
        year++;
        month -= 12;
    }

    calculate();
}

calculate();
