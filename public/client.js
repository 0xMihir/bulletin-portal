$ = function(e) {
    return document.querySelector(e);
};
_ = function(e) {
    return document.querySelectorAll(e);
};

menu = new mdc.menu.MDCMenu($("#account-dropdown"));
var actionItems = _(".mdc-icon-button");
for (const i in actionItems) {
    if (actionItems.hasOwnProperty(i)) {
        const e = actionItems[i];
        new mdc.ripple.MDCRipple(e).unbounded = true;
    }
}
mdc.autoInit();

$("#menu-button").addEventListener("click", e => {
    menu.open = !menu.open;
});

// if (location.protocol != 'https:')
// {
//  location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
// }
var showCreate = () => {
    var createElement = document.createElement("a");
    createElement.id = "createElement";
    createElement.href = "/create";
    createElement.className =
        "mdc-fab mdc-fab--exited mdc-fab--extended app-fab--absolute";
    createElement.innerHTML =
        '<div class="mdc-fab__ripple"></div><span class="material-icons mdc-fab__icon">add</span> <span class="mdc-fab__label">Create</span>';
    $("#menu-button").insertAdjacentHTML(
        "beforebegin",
        createElement.outerHTML
    );
    setTimeout(() => {
        $("#createElement").classList.remove("mdc-fab--exited");
    }, 250);
};

function start() {
    gapi.load("auth2", () => {
        auth2 = gapi.auth2.init({
            client_id:
                "449186826108-aq9kjo6tikbosc3ltt5qmpae093v64kq.apps.googleusercontent.com",
            ux_mode: "redirect",
            hosted_domain: "edgemont.org"
        });
    });
    const options = {
        method: "post",
        headers: {
            "Content-Type": "application/json",
            "X-Requested-With": "Fetch"
        }
    };
    if (localStorage.signedIn == "true") {
        fetch("/verifyToken", options).then(async e => {
            const json = await e.json();
            if (json.status == "success") {
                signInButton.innerHTML = "Logout";
                0 / 0;
                signInButton.parentElement.removeEventListener("click", signIn);
                signInButton.parentElement.addEventListener("click", signOut);
                if (window.location.pathname == "/") {
                    showCreate();
                }
            } else {
                signOut();
            }
        });
    }
}

var signInButton = $("#signInButton");

function signIn() {
    if (localStorage.signedIn == "false" || !localStorage.signedIn) {
        auth2
            .grantOfflineAccess()
            .then(e => {
                const options = {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json",
                        "X-Requested-With": "Fetch"
                    },
                    body: JSON.stringify(e)
                };
                fetch("/authenticate", options).then(async e => {
                    const json = await e.json();
                    if (json.status == "success") {
                        signInButton.innerHTML = "Sign out";
                        signInButton.parentElement.removeEventListener(
                            "click",
                            signIn
                        );
                        signInButton.parentElement.addEventListener(
                            "click",
                            signOut
                        );
                        localStorage.signedIn = true;
                        window.location.reload();
                    } else {
                        if (json.error === "incorrectDomain") {
                            new mdc.snackbar.MDCSnackbar(
                                document.querySelector("#domain-error")
                            ).open();
                        } else {
                            signOut();
                        }
                    }
                });
            })
            .catch(e => {});
    }
}

function editAnnouncement(e) {
    const id = e.getAttribute("data-id");
    if (!id) return;

    window.open("/edit?id=" + id, "_self");
}

function deleteAnnouncement(e) {
    const id = e.getAttribute("data-id");
    if (!id) return;

    const options = {
        method: "delete",
        headers: {
            "Content-Type": "application/json",
            "X-Requested-With": "Fetch",
            redirect: "follow"
        }
    };
    fetch(`/announcements/${id}`, options)
        .then(() => {
            let card = $(`*[data-id="${id}"`);
            card.parentElement.removeChild(card);
            new mdc.snackbar.MDCSnackbar(
                document.querySelector("#delete-success")
            ).open();
        })
        .catch(e => {
            console.error(e);
        });
    fetch("/announcements");
}

function signOut() {
    localStorage.signedIn = false;
    if ($("#createElement"))
        $("#createElement").classList.remove("mdc-fab--exited");
    signInButton.innerHTML = "Login";
    gapi.auth.setToken(null);
    gapi.auth.signOut();
    setTimeout(() => {
        const options = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "X-Requested-With": "Fetch",
                redirect: "follow"
            }
        };
        fetch("/logout", options)
            .catch(e => {
                console.error(e);
            })
            .then(() => {
                window.location.reload();
            });
    }, 180);
}

signInButton.parentElement.addEventListener("click", () => {
    signIn();
});
if ("serviceWorker" in navigator) {
    navigator.serviceWorker.register("/serviceWorker.js").then(async e => {
        const subscription = await e.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: Uint8Array.from([
                4,
                218,
                232,
                34,
                142,
                99,
                145,
                93,
                3,
                176,
                168,
                247,
                65,
                242,
                115,
                239,
                157,
                178,
                166,
                114,
                211,
                42,
                92,
                108,
                105,
                137,
                153,
                145,
                166,
                143,
                235,
                199,
                29,
                69,
                107,
                248,
                197,
                189,
                210,
                106,
                91,
                143,
                54,
                6,
                161,
                231,
                77,
                102,
                163,
                203,
                76,
                169,
                71,
                118,
                13,
                129,
                4,
                247,
                10,
                120,
                243,
                88,
                56,
                132,
                144
            ])
        });
        fetch("/subscribe", {
            method: "POST",
            body: JSON.stringify(subscription),
            headers: {
                "content-type": "application/json"
            }
        });
    });
}

function populate(json) {
    const cards = _(".mdc-card");
    var existing = [];
    for (const i in cards) {
        if (cards.hasOwnProperty(i)) {
            const card = cards[i];
            existing.push(parseInt(card.getAttribute("data-id")));
        }
    }
    json.forEach(e => {
        if (!existing.includes(e.id)) {
            $(".card-wrapper").innerHTML += `<div class="mdc-card" data-id="${
                e.id
            }">
                <div class="mdc-card-wrapper__text-section" tabindex="0">
                    <div class="mdc-typography--headline6"> ${e.title}</div>
                    <div class="mdc-typography--subtitle1">  Posted by ${
                        e.creatorName
                    } <br> ${moment.unix(e.timestamp).format("LLLL")}
                    </div>
                </div>
                <div class="mdc-card-wrapper__text-section" tabindex="0">
                    <span class="mdc-typography--body1">${e.description}</span>
                </div>
                <div class="mdc-card__actions">
                    <div class="mdc-card__action-icons">
                <a target="_blank" title="${e.creatorEmail}" href="mailto:${
                e.creatorEmail
            }" class="material-icons mdc-icon-button mdc-card__action mdc-card__action--icon">mail</a>
              ${
                  localStorage.signedIn == "true"
                      ? `
                        <button onclick="editAnnouncement(this)" data-id="${e.id}"
                            class="material-icons mdc-icon-button mdc-card__action mdc-card__action--icon"
                            title="Edit announcement">edit</button>
                        <button onclick="deleteAnnouncement(this)" data-id="${e.id}"
                            class="material-icons mdc-icon-button mdc-card__action mdc-card__action--icon"
                            title="Delete announcement">delete</button>
                `
                      : ""
              }
                    </div>
                </div>
            </div>`;
        }
    });
}

function update() {
    if (window.location.pathname !== "/") return;
    var networkDataReceived = false;
    var networkUpdate = fetch("/announcements" + location.search)
        .then(async e => {
            json = await e.json();
            $(".card-wrapper").innerHTML = "";
            populate(json);
            const x = document.querySelector(".mdc-linear-progress");
            if (x) x.MDCLinearProgress.close();
            networkDataReceived = true;
        })
        .catch(e => {
            console.error(e);
            const x = document.querySelector(".mdc-linear-progress");
            if (x) x.MDCLinearProgress.close();
            new mdc.snackbar.MDCSnackbar($("#offline")).open();
        });
    caches
        .match("/announcements")
        .then(response => {
            if (!response) throw Error("No data");
            return response.json();
        })
        .then(json => {
            if (!networkDataReceived) {
                document.querySelector(
                    ".mdc-linear-progress"
                ).MDCLinearProgress.determinate = false;
                populate(json);
            }
        })
        .catch(() => {
            return networkUpdate;
        })
        .catch(e => {
            console.error(e);
        });
}

update();
function openCalendar() {
    window.open("/calendar", "_self");
}
