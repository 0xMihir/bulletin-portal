module.exports = {
    token: "accessToken",
    announcementsTableName: "announcements",
    usersTableName: "users",
    subscriptionsTableName: "subscriptions",
    timeUnitsInADay: 86400
};
