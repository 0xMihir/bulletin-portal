const express = require("express");
const path = require("path");
var fs = require("fs");
var googleAuth = require("./googleAuth");
var jwt = require("jsonwebtoken");
var sqlite3 = require("sqlite3").verbose();
const webPush = require("web-push");
const moment = require("moment-timezone");
const router = express.Router();
require("dotenv").config();
const constants = require("./constants");
const privateKey = fs.readFileSync("./private.key", "utf8");
const publicKey = fs.readFileSync("./public.key", "utf8");

var file = path.join(__dirname, "/database.db");
var exists = fs.existsSync(file);

if (!exists) {
    fs.openSync(file, "w");
}
var db = new sqlite3.Database(file);

if (!exists) {
    db.run(
        `CREATE TABLE ${constants.usersTableName} (userID CHAR, googleID CHAR, realName CHAR, email CHAR)`
    );
    db.run(
        `CREATE TABLE ${constants.announcementsTableName} (id integer primary key , timestamp INTEGER, title CHAR, description CHAR, creatorName CHAR, creatorEmail CHAR)`
    );
    db.run(
        `CREATE TABLE ${constants.subscriptionsTableName} (endpoint CHAR, p256dh CHAR,  auth CHAR, UNIQUE( p256dh, auth))`
    );
}

const vapid = JSON.parse(fs.readFileSync(__dirname + "/vapid.json").toString());
webPush.setVapidDetails(vapid.subject, vapid.publicKey, vapid.privateKey);

function setCookie(token, res) {
    const cookieOptions = {
        httpOnly: true,
        maxAge: 9000000
    };
    res.cookie(constants.token, token, cookieOptions);
}

function notify(body) {
    const payload = JSON.stringify({
        title: "New Bulletin Announcement",
        body: body
    });
    db.all(`SELECT * FROM subscriptions`, (e, r) => {
        r.forEach(e => {
            webPush
                .sendNotification(
                    {
                        endpoint: e.endpoint,
                        keys: {
                            auth: e.auth,
                            p256dh: e.p256dh
                        }
                    },
                    payload
                )
                .catch(error => {
                    if (error.name == "WebPushError") {
                        db.run(
                            `DELETE FROM ${constants.subscriptionsTableName} WHERE endpoint = ?`,
                            error.endpoint
                        );
                    }
                });
        });
    });
}

function checkAuth(req, res, next) {
    if (process.env.production === "false") {
        next(true, { realName: "Test User", email: "Test" });
        return;
    }
    const token = req.cookies.accessToken;
    if (!token) {
        try {
            res.clearCookie(constants.token, {
                path: "/"
            });
        } catch (e) {
            //don't log http headers set error
        }
        next(false);
        return;
    }
    jwt.verify(token, publicKey, (e, p) => {
        if (e) {
            try {
                res.clearCookie(constants.token, {
                    path: "/"
                });
            } catch (e) {
                //don't log http headers set error
            }
            next(false);
        } else {
            db.get(
                `SELECT * FROM ${constants.usersTableName} WHERE userID = ?`,
                p.sub,
                (e, r) => {
                    if (!r) {
                        try {
                            res.clearCookie(constants.token, {
                                path: "/"
                            });
                        } catch (e) {
                            //don't log http headers set error
                        }
                        next(false, r);
                    } else {
                        next(true, r);
                    }
                }
            );
        }
    });
}

var current_bulletin = [];

var recalculateBulletin = cb => {
    timeOfLastUpdate = Date.now();
    db.all(
        `SELECT * FROM ${constants.announcementsTableName} WHERE date(timestamp,'unixepoch') = date('now')
    `,
        (e, rows) => {
            if (e) {
                console.error(e);
                return;
            }
            current_bulletin = rows;
            if (cb) {
                cb(rows);
            }
        }
    );
};

recalculateBulletin();

router.delete("/announcements/:id", (req, res) => {
    checkAuth(req, res, success => {
        if (success) {
            db.run(
                `DELETE FROM ${constants.announcementsTableName} WHERE id = ?`,
                req.params.id
            );
            // req.flash('message', 'Thank you for deleting your announcement');
            recalculateBulletin();
            res.json({
                status: "success"
            });
        }
    });
});

router.get('/.well-known/assetlinks.json', (req, res) => {
	res.sendFile(path.join(__dirname, 'assetlinks.json'))
})

var triggerCount = 0;
var lastRefresh = new Date().getTime();

router.get("/", (req, res) => {
    if(new Date().getTime() - lastRefresh >= 60000){
        recalculateBulletin();
        lastRefresh = new Date().getTime()
    }

    console.log("get request")
    var timestamp = req.query.timestamp;
    console.log(timestamp)
    if (!timestamp) {
        checkAuth(req, res, success => {
            console.log(success)
            res.render("index.ejs", {
                admin: success,
                moment: moment,
                entries: current_bulletin,
                homePage: true
            });
            return;
        });
        return;
    } else {
        db.all(
            `SELECT * FROM ${constants.announcementsTableName} WHERE date(timestamp,'unixepoch') = date(${timestamp},'unixepoch')`,
            (e, rows) => {
                if (e) {
                    console.error(e);
                    return;
                }
                checkAuth(req, res, success => {
                    res.status(200).render("index.ejs", {
                        admin: success,
                        moment: moment,
                        entries: rows,
                        homePage: true
                        // message: req.flash('message')
                    });
                    return;
                });
            }
        );
    }
});

router.get("/calendar", (req, res) => {
    res.render("calendar", {
        create: true,
        errors: [],
        title: "",
        description: "",
        date: ""
    });
});

/******************************** PATHS ********************************************** */
router.get("/create", (req, res) => {
    checkAuth(req, res, success => {
        if (!success) {
            res.status(401).render("error", {
                code: "401",
                reason: "Unauthorized",
                description:
                    "You do not have access to this page because you are not signed in. To sign in, click the person at the top right corner."
            });
            return;
        }
        res.render("create", {
            create: true,
            errors: [],
            title: "",
            description: "",
            date: ""
        });
    });
});

router.get("/edit", (req, res) => {
    checkAuth(req, res, success => {
        if (!success) {
            res.status(401).render("error", {
                code: "401",
                reason: "Unauthorized",
                description:
                    "You do not have access to this page because you are not signed in. To sign in, click the person at the top right corner."
            });
            return;
        }
        db.all(
            `SELECT * FROM ${constants.announcementsTableName} WHERE id = ?`,
            [req.query.id],
            (e, row) => {
                if (e) {
                    console.error(e);
                    return;
                }
                if (row.length == 0) {
                    res.status(404).render("error", {
                        code: "404",
                        reason: "Announcement Not Found",
                        description:
                            "The bulletin announcement you are trying to edit does not exist. It may have been deleted."
                    });
                    return;
                }
                res.render("create", {
                    create: false,
                    postId: req.query.id,
                    data: row
                });
            }
        );
    });
});

let valid = data => {
    return data.length > 0;
};

let dateValid = date => {
    return moment(date).isValid;
};
/******************************** API ********************************************** */

router.get("/announcements", (req, res) => {
    if (!req.query.timestamp) 
        res.json(current_bulletin);
    else{
        var timestamp = req.query.timestamp;
        timestamp = timestamp / 1000;
        db.all(
            `SELECT * FROM ${constants.announcementsTableName} WHERE date(timestamp,'unixepoch') = date(${timestamp},'unixepoch')`,
            (e, rows) => {
                if (e) {
                    console.error(e);
                    return;
                }
                res.json(rows);
            }
        );
    }
});

function insertAnnouncement(timestamp, title, description, realName, email) {
    db.run(
        `INSERT INTO '${constants.announcementsTableName}' (timestamp, title, description, creatorName, creatorEmail) VALUES (?, ?, ?, ?, ?);`,
        [timestamp, title, description, realName, email],
        (e, r) => {
            if (e) {
                throw e;
            }
        }
    );
}

router.post("/announcements", (req, res) => {
    checkAuth(req, res, (success, data) => {
        if (!success) {
            res.status(403).json({
                status: "error",
                error: "unauthorized"
            });
            return;
        }
        const timestamp = req.body.timestamp;
        const end_timestamp = req.body.end_timestamp;
        const title = req.body.title;
        const description = req.body.description;

        const errors = [];
        //Validate data
        if (!valid(title)) errors.push("Please enter a title");
        if (!valid(description)) errors.push("Please enter a description");
        if (!dateValid(timestamp)) errors.push("Please enter a valid date");
        if (errors.length > 0) {
            res.render("create", {
                title: title,
                date: timestamp,
                description: description,
                errors: errors
            });
            return;
        }
        if (!end_timestamp || end_timestamp < timestamp) {
            end_timestamp = timestamp;
        }
        console.log(timestamp);
        console.log("to");
        console.log(end_timestamp);
        console.log("Begin times");
        for (
            var currentTimestamp = timestamp;
            currentTimestamp <= end_timestamp;
            currentTimestamp += constants.timeUnitsInADay
        ) {
            console.log(currentTimestamp);
            try {
                insertAnnouncement(
                    currentTimestamp,
                    title,
                    description,
                    data.realName,
                    data.email
                );
            } catch (e) {
                res.render("error.ejs");
                return;
            }
            notify(title);
        }
        console.log("end times");
        recalculateBulletin();
        res.send({
            url: "/"
        });
    });
});

router.patch("/announcements/:id", (req, res) => {
    checkAuth(req, res, success => {
        if (success) {
            db.all(
                "SELECT * FROM announcements WHERE id = ?",
                req.params.id,
                (e, r) => {
                    if (r.length == 0) {
                        res.status(404).json({
                            error: "postNotFound"
                        });
                        return;
                    }
                    db.run(
                        `UPDATE announcements SET timestamp = ?, title = ?, description = ?  WHERE id = ?`,
                        [
                            req.body.timestamp,
                            req.body.title,
                            req.body.description,
                            req.params.id
                        ]
                    );
                }
            );
            // // req.flash('message', 'Thank you for deleting your announcement');
            // recalculateBulletin()
            res.send({
                status: "success",
                url: "/"
            });
        }
    });
});

router.delete("/announcements/:id", (req, res) => {
    checkAuth(req, res, success => {
        if (success) {
            db.run(
                `DELETE FROM ${constants.announcementsTableName} WHERE id = ?`,
                req.params.id
            );
            // req.flash('message', 'Thank you for deleting your announcement');
            recalculateBulletin();
            res.json({
                status: "success"
            });
        }
    });
});
/******************************** AUTHENTICATION ********************************************** */

router.post("/authenticate", async (req, res) => {
    if (!req.headers["x-requested-with"]) {
        res.status(403).json({
            status: "error",
            error: "csrf"
        });
        return;
    }

    if (process.env.production === "false") {
        //sign a fake payload
        var payload = {
            name: "Developer :D"
        };

        var signOptions = {
            issuer: "Edgemont Jr./Sr. High School",
            subject: r.userID,
            audience: "https://ehs.edgemont.org",
            expiresIn: "12h",
            algorithm: "RS256"
        };

        const token = jwt.sign(payload, privateKey, signOptions);
        setCookie(token, res);
        return res.json({
            status: "success",
            accessToken: token
        });
    }

    const accountData = await googleAuth.getGoogleAccountFromCode(
        req.body.code
    );
    const data = accountData.profile.data;

    if (
        (data.verified_email === true && data.hd === "edgemont.org") ||
        data.email == "mpatil@student.edgemont.org" ||
        data.email == "kenge@student.edgemont.org"
    ) {
        db.get(
            `SELECT * FROM ${constants.usersTableName} WHERE googleID = ?`,
            data.id,
            (e, r) => {
                if (!r) {
                    db.run(
                        `INSERT OR IGNORE INTO ${constants.usersTableName} (email, googleID, realName, userID) VALUES (?, ?, ?, ?);`,
                        [data.email, data.id, data.name]
                    );

                    var payload = {
                        name: data.name
                    };

                    var signOptions = {
                        issuer: "Edgemont Jr./Sr. High School",
                        subject: id,
                        audience: "https://ehs.edgemont.org",
                        expiresIn: "12h",
                        algorithm: "RS256"
                    };

                    const token = jwt.sign(payload, privateKey, signOptions);
                    setCookie(token, res);

                    return res.json({
                        status: "success",
                        accessToken: token
                    });
                }

                var payload = {
                    name: r.realName
                };

                var signOptions = {
                    issuer: "Edgemont Jr./Sr. High School",
                    subject: r.userID,
                    audience: "https://ehs.edgemont.org",
                    expiresIn: "12h",
                    algorithm: "RS256"
                };

                const token = jwt.sign(payload, privateKey, signOptions);
                setCookie(token, res);
                return res.json({
                    status: "success",
                    accessToken: token
                });
            }
        );
    } else {
        res.statusCode = 403;
        res.json({
            error: "incorrectDomain"
        });
    }
});

router.post("/verifyToken", (req, res) => {
    checkAuth(req, res, e => {
        if (e) {
            res.json({
                status: "success"
            });
        } else {
            res.status(401).json({
                error: "unauthorized"
            });
        }
    });
});

router.post("/logout", (req, res) => {
    res.clearCookie(constants.token, {
        path: "/"
    });

    res.json({
        status: "success"
    });
});

router.post("/subscribe", (req, res) => {
    const subscription = req.body;
    res.status(201).json({});
    db.run(
        `INSERT OR IGNORE INTO '${constants.subscriptionsTableName}' (endpoint, p256dh, auth) VALUES (?, ?, ?)`,
        [
            subscription.endpoint,
            subscription.keys.p256dh,
            subscription.keys.auth
        ]
    );
});

module.exports = router;
