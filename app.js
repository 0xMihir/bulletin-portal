const express = require("express");
const flash = require("express-flash");
const fs = require("fs");
const path = require("path");
const bearerToken = require("express-bearer-token");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const session = require("express-session");
require("dotenv").config();
const router = require("./router");

const app = express();

const middleware = [
    express.static("public"),
    bodyParser.json(),
    bodyParser.urlencoded({ extended: true }),
    bearerToken(),
    cookieParser(),
    flash(),
    session({
        cookie: {
            maxAge: 60000
        },
        secret: process.env.secret,
        resave: false,
        saveUninitialized: false
    })
];

app.use(middleware);
express.static("public");
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use((req, res, next) => {
    res.set('Cache-Control', 'no-store, no-cache, must-revalidate, private')
    next()
})

app.use("/", router);

//Failsafe for unauthorized pages
app.use((req, res, next) => {
    res.status(404).render("error", {
        code: "404",
        reason: "Page Not Found",
        description: "The page you are looking for does not exist."
    });
});

app.listen(8080);
