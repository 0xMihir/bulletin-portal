var fs = require("fs");
var { google } = require("googleapis");
const secret = JSON.parse(
    fs.readFileSync(__dirname + "/client_secret.json").toString()
);

const googleConfig = {
    clientId: secret.web.client_id,
    clientSecret: secret.web.client_secret,
    redirect: "https://edgemontbulletin.tech"
};

function createConnection() {
    return new google.auth.OAuth2(
        googleConfig.clientId,
        googleConfig.clientSecret,
        googleConfig.redirect
    );
}

function getInfo(auth) {
    return new Promise((resolve, reject) => {
        if (!auth) reject(new Error("No Auth passed in"));

        google.oauth2("v2").userinfo.v2.me.get(
            {
                auth: auth
            },
            (e, p) => {
                resolve(p);
            }
        );
    });
}
async function getGoogleAccountFromCode(code) {
    const auth = createConnection();
    const data = await auth.getToken(code);
    const tokens = data.tokens;
    auth.setCredentials(tokens);
    const profile = await getInfo(auth);
    return {
        data: data,
        profile: profile
    };
}

module.exports = {
    getGoogleAccountFromCode: getGoogleAccountFromCode
};
